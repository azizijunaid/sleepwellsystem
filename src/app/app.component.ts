import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '../../node_modules/angularfire2/auth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'questionApp';

  constructor(private router: Router, private firebaseAuth: AngularFireAuth){
    if(firebaseAuth.user){
      router.navigate(['/question']);
      return;
    }
    router.navigate(['/login']);
  }
}
