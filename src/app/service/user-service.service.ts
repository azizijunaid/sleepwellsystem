import { Injectable } from '@angular/core';
import { AngularFireAuth } from '../../../node_modules/angularfire2/auth';
import { User } from '../shared/interfaces/user.interface';
import { CookieService } from '../../../node_modules/ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(
    private afs: AngularFireAuth,
    private cookieService: CookieService
  ) { }

  async createUser(user: User){
     try {
        return await this.afs.auth.createUserWithEmailAndPassword(user.email, user.password);
     } catch (error) {
       throw {error: 'some error'}
     }
  }

  async login(user){
    try {
      const userData = await this.afs.auth.signInWithEmailAndPassword(user.email, user.password)
      this.cookieService.set('uid', userData.user.uid);
    } catch (error) {
      throw {error: 'auth failed error'}
    }
  }

  async logout(){
    try {
      await this.afs.auth.signOut();
    } catch (error) {
      throw {error: 'auth failed error'}
    }
  }
}
