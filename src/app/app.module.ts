import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { QuestionComponent } from './question/question.component';
import { RouterModule, Routes } from '@angular/router';
import { UserServiceService } from './service/user-service.service';
import { FormsModule } from '@angular/forms';
import { firebaseConfig } from './credentials/credentials';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { CookieService } from 'ngx-cookie-service';


const routes: Routes = [
  { path: "signup", component: SignupComponent },
  { path: "login", component: LoginComponent },
  { path: "question", component: QuestionComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    QuestionComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    Ng4LoadingSpinnerModule,
    Ng4LoadingSpinnerModule.forRoot(),
    RouterModule.forRoot(routes),
    FormsModule
  ],
  providers: [UserServiceService, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
