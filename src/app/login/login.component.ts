import { Component, OnInit } from '@angular/core';
import { User } from '../shared/interfaces/user.interface';
import { UserServiceService } from '../service/user-service.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User;
  constructor(private router: Router,
    private userService: UserServiceService, 
    private spinner : Ng4LoadingSpinnerService
  ) { }

  ngOnInit() {
    this.user = {
      email: '',    
      password: ''
    }
  }

  navigateToSignup(){
    this.router.navigate(['/signup']);
  }

  async login(){
    try {
      this.spinner.show();
      const userData = await this.userService.login(this.user);
      console.log('userData', userData);
      //this.cookieService.set('uid', '1')
      this.spinner.hide();
      this.router.navigate(['/question']);
    } catch (error) {
      this.spinner.hide();
      console.log(error);
    }
  }
}
