import { Component, OnInit } from '@angular/core';
import { User } from '../shared/interfaces/user.interface';
import { UserServiceService } from '../service/user-service.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  user: User;
  constructor(
    private userService: UserServiceService, 
    private spinner: Ng4LoadingSpinnerService,
    private router: Router
  ) { }

  ngOnInit() {
    this.user = {
      email: '',    
      password: ''
    }
  }

  async signup(){
    try {
      this.spinner.show()
      await this.userService.createUser(this.user);
      this.spinner.hide()
      this.router.navigate(['login'])
    } catch (error) {
      this.spinner.hide()
      console.log(error);
    }
  }
}
