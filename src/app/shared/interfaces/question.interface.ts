export interface question{
    id?:string;
    maxValue?: number;
    minValue?: number;
    step: number;
    text: string;
    type: string;
    slider: string;
    choices?: string[];
    choiceValue?: number[];
    answer?: string
}