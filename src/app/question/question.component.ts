import { Component, OnInit } from '@angular/core';
import { question } from '../shared/interfaces/question.interface';
import { AngularFirestore } from '../../../node_modules/angularfire2/firestore';
import { UserServiceService } from '../service/user-service.service';
import { Router } from '../../../node_modules/@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { CookieService } from '../../../node_modules/ngx-cookie-service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  
  questions = [];
  constructor(
    private fDB: AngularFirestore, 
    private userService: UserServiceService, 
    private router: Router,
    private cookieService: CookieService,
    private spinner: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
   this.fDB.collection('Questions').snapshotChanges().subscribe(questions => {
      for(let i = 0; i < questions.length; i++){
        const data = questions[i]['payload']['doc'].data() as question;
        data.id = questions[i]['payload']['doc']['id'];
        data.answer = '';
        this.questions.push(data);
      }

      this.spinner.hide()
      console.log(this.questions);
    });
  }

  submit(){
    const answers = {};
    this.spinner.show();
    for (let index = 0; index < this.questions.length; index++) {
      if(this.questions[index].answer == ""){
        return
      }

      answers['answerValue'] = this.questions[index].answer
      answers['questionId'] = this.questions[index].id,
      answers['uid'] = this.cookieService.get('uid');
      answers['createdAt'] = new Date();
    }

    this.addAnswer(answers)
    console.log('ans', answers);
  }

  async addAnswer(answer){
    try {
      console.log(answer);
      const data = await this.fDB.collection('UserAnswers').add(answer);
      this.spinner.hide();
      console.log('data');
    } catch (error) {
      this.spinner.hide();
      console.log(error);
    }
  }

  logout(){
    this.userService.logout();
    this.router.navigate(['/login']);
  }

}
